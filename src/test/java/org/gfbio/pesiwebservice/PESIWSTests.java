package org.gfbio.pesiwebservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import javax.json.JsonObject;
import org.gfbio.config.WSConfiguration;
import org.gfbio.util.YAMLConfigReader;
import org.junit.Test;

public class PESIWSTests {

  @Test
  public void testIsResponding() {
    assertTrue(new PESIWSServiceImpl().isResponding());
  }

  @Test
  public void testReadConfiguration() {
    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    WSConfiguration config = reader.getWSConfig("PESI");

    System.out.println(config.toString());
  }

  @Test
  public void WSSynonymsTest() {
    System.out.println("*** Test synonym service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getSynonyms("urn:lsid:marinespecies.org:taxname:105847").create();
    assertEquals(0, check.size());
  }

  @Test
  public void WSSynonyms2Test() {
    System.out.println("*** Test synonym service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getSynonyms("urn:lsid:faunaeur.org:taxname:95299").create();
    assertNotNull(check);
    System.out.println(check.toString());
  }

  @Test
  public void WSSearchTest() {
    System.out.println("*** Test search service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.search("Helianthus", "exact").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSHierarchy() {
    System.out.println("*** Test hierarchy service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getHierarchy("urn:lsid:faunaeur.org:taxname:95076").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSallbroader() {
    System.out.println("*** Test allbroader service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getAllBroader("urn:lsid:indexfungorum.org:names:161267").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermC() {
    System.out.println("*** Test combined term service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getTermInfosCombined("urn:lsid:indexfungorum.org:names:161267").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermO() {
    System.out.println("*** Test original term service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getTermInfosOriginal("urn:lsid:indexfungorum.org:names:161267").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermP() {
    System.out.println("*** Test processed term service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check =
        serviceIMPL.getTermInfosProcessed("urn:lsid:indexfungorum.org:names:161267").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSMetadata() {
    System.out.println("*** Test metadata service ***");
    PESIWSServiceImpl serviceIMPL = new PESIWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getMetadata().create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

}

