package org.gfbio.pesiwebservice;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;

/**
 * Webservice implementation in the GFBio Terminology Service. This class implements all necessary
 * methods for the TS endpoints according to the WSInterface defined in the gfbioapi project.
 * 
 * @author nkaram <A HREF="mailto:naouel.karam@fu-berlin.de">Naouel Karam</A>
 * 
 */
public class PESIWSServiceImpl implements WSInterface {

  private static final Logger LOGGER = Logger.getLogger(PESIWSServiceImpl.class);

  private final String taxonURLPrefix = "http://www.eu-nomen.eu/portal/taxon.php?GUID=";

  private PESIDAO pesidao;

  // properties from YAML file
  private WSConfiguration wsConfig;

  public PESIWSServiceImpl() {

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    wsConfig = reader.getWSConfig("PESI");

    this.pesidao = new PESIDAO();
    LOGGER.info(" a PESI webservice is ready to use");
  }

  @Override
  public String getAcronym() {
    return wsConfig.getAcronym();
  }

  @Override
  public String getDescription() {
    return wsConfig.getDescription();
  }

  @Override
  public String getName() {
    return wsConfig.getName();
  }

  @Override
  public String getURI() {
    return this.taxonURLPrefix;
  }

  @Override
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  @Override
  public String getCurrentVersion() {
    return wsConfig.getVersion();
  }

  /**
   * Implements the getAllBroader method of the WSInterface Gets all broader terms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("AllBroader query " + externalID);
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("pesi");
    PESIRecord rec = this.pesidao.getPESIRecordbyGUID(externalID);
    String rank = rec.getRank();
    AllBroaderResultSetEntry e;
    if (!rank.equals("Kingdom")) {
      e = new AllBroaderResultSetEntry();
      e.setRank("Kingdom");
      e.setLabel(rec.getKingdom());
      PESIRecord[] level = this.pesidao.searchByNameExact(rec.getKingdom());
      String levelURI = level[0].getUrl();
      e.setUri(levelURI);
      e.setExternalID(level[0].getGUID());
      rs.addEntry(e);
    }
    if (!rank.equals("Phylum")) {
      e = new AllBroaderResultSetEntry();
      e.setRank("Phylum");
      e.setLabel(rec.getPhylum());
      PESIRecord[] level = this.pesidao.searchByNameExact(rec.getPhylum());
      String levelURI = level[0].getUrl();
      e.setUri(levelURI);
      e.setExternalID(level[0].getGUID());
      rs.addEntry(e);
    }
    if (!rank.equals("Class")) {
      e = new AllBroaderResultSetEntry();
      e.setRank("Class");
      e.setLabel(rec.getPhylum());
      PESIRecord[] level = this.pesidao.searchByNameExact(rec.get_class());
      String levelURI = level[0].getUrl();
      e.setUri(levelURI);
      e.setExternalID(level[0].getGUID());
      rs.addEntry(e);
    }
    if (!rank.equals("Order")) {
      e = new AllBroaderResultSetEntry();
      e.setRank("Order");
      e.setLabel(rec.getPhylum());
      PESIRecord[] level = this.pesidao.searchByNameExact(rec.getOrder());
      String levelURI = level[0].getUrl();
      e.setUri(levelURI);
      e.setExternalID(level[0].getGUID());
      rs.addEntry(e);
    }
    if (!rank.equals("Family")) {
      e = new AllBroaderResultSetEntry();
      e.setRank("Family");
      e.setLabel(rec.getPhylum());
      PESIRecord[] level = this.pesidao.searchByNameExact(rec.getFamily());
      String levelURI = level[0].getUrl();
      e.setUri(levelURI);
      e.setExternalID(level[0].getGUID());
      rs.addEntry(e);
    }
    if (!rank.equals("Genus")) {
      e = new AllBroaderResultSetEntry();
      e.setRank("Genus");
      e.setLabel(rec.getPhylum());
      PESIRecord[] level = this.pesidao.searchByNameExact(rec.getGenus());
      String levelURI = level[0].getUrl();
      e.setUri(levelURI);
      e.setExternalID(level[0].getGUID());
      rs.addEntry(e);
    }
    return rs;
  }

  /**
   * Implements the getHierarchy method of the WSInterface Gets the broader hierarchy of a taxon
   * given its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("hierarchy query " + externalID);
    GFBioResultSet<HierarchyResultSetEntry> rs =
        new GFBioResultSet<HierarchyResultSetEntry>("pesi");
    PESIRecord rec = this.pesidao.getPESIRecordbyGUID(externalID);
    HierarchyResultSetEntry e = new HierarchyResultSetEntry();
    ArrayList<String> array = new ArrayList<String>();
    String kingdomURI = null;
    if (rec.getKingdom() != null) {
      e.setLabel(rec.getKingdom());
      e.setRank("Kingdom");
      PESIRecord[] kingdom = this.pesidao.searchByNameExact(rec.getKingdom());
      kingdomURI = kingdom[0].getUrl();
      e.setUri(kingdomURI);
      e.setExternalID(kingdom[0].getGUID());
      e.setHierarchy(array);
      rs.addEntry(e);
    }
    String phylumURI = null;
    if (rec.getPhylum() != null) {
      e = new HierarchyResultSetEntry();
      array = new ArrayList<String>();
      array.add(kingdomURI);
      e.setLabel(rec.getKingdom());
      e.setRank("Phylum");
      PESIRecord[] phylum = this.pesidao.searchByNameExact(rec.getPhylum());
      phylumURI = phylum[0].getUrl();
      e.setUri(phylumURI);
      e.setExternalID(phylum[0].getGUID());
      e.setHierarchy(array);
      rs.addEntry(e);
    }
    String classURI = null;
    if (rec.get_class() != null) {
      e = new HierarchyResultSetEntry();
      array = new ArrayList<String>();
      array.add(phylumURI);
      e.setLabel(rec.get_class());
      e.setRank("Class");
      PESIRecord[] _class = this.pesidao.searchByNameExact(rec.get_class());
      classURI = _class[0].getUrl();
      e.setUri(classURI);
      e.setExternalID(_class[0].getGUID());
      e.setHierarchy(array);
      rs.addEntry(e);
    }
    String orderURI = null;
    if (rec.getOrder() != null) {
      e = new HierarchyResultSetEntry();
      array = new ArrayList<String>();
      array.add(classURI);
      e.setLabel(rec.getOrder());
      e.setRank("Order");
      PESIRecord[] order = this.pesidao.searchByNameExact(rec.getOrder());
      orderURI = order[0].getUrl();
      e.setUri(orderURI);
      e.setExternalID(order[0].getGUID());
      e.setHierarchy(array);
      rs.addEntry(e);
    }
    String familyURI = null;
    if (rec.getFamily() != null) {
      e = new HierarchyResultSetEntry();
      array = new ArrayList<String>();
      array.add(orderURI);
      e.setLabel(rec.getFamily());
      e.setRank("Family");
      PESIRecord[] family = this.pesidao.searchByNameExact(rec.getFamily());
      familyURI = family[0].getUrl();
      e.setUri(familyURI);
      e.setExternalID(family[0].getGUID());
      e.setHierarchy(array);
      rs.addEntry(e);
    }
    String genusURI = null;
    if (rec.getGenus() != null) {
      e = new HierarchyResultSetEntry();
      array = new ArrayList<String>();
      array.add(familyURI);
      e.setLabel(rec.getGenus());
      e.setRank("Genus");
      PESIRecord[] genus = this.pesidao.searchByNameExact(rec.getGenus());
      genusURI = genus[0].getUrl();
      e.setUri(genusURI);
      e.setExternalID(genus[0].getGUID());
      e.setHierarchy(array);
      rs.addEntry(e);
    }
    return rs;
  }

  /**
   * Implements the getSynonyms method of the WSInterface Gets the list of synonyms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("synonym query " + externalID);
    GFBioResultSet<SynonymsResultSetEntry> rs = new GFBioResultSet<SynonymsResultSetEntry>("pesi");
    PESIRecord[] synonyms = this.pesidao.getPESISynonyms(externalID);
    SynonymsResultSetEntry e = new SynonymsResultSetEntry();
    ArrayList<String> array = new ArrayList<String>();
    if (synonyms != null) {
      for (PESIRecord synonym : synonyms) {
        StringBuilder name = new StringBuilder();
        if (synonym.getScientificname() != null) {
          name.append(synonym.getScientificname());
        }
        if (synonym.getAuthority() != null && name.toString() != null) {
          name.append(synonym.getAuthority());
        }
        if (name.toString() != null) {
          array.add(name.toString());
        }
      }
      e.setSynonyms(array);
      rs.addEntry(e);
      LOGGER.info("synonym query processed " + externalID);
    }
    return rs;
  }

  /**
   * Implements the getTermInfosOriginal method of the WSInterface Gets the informations of a taxon
   * given its URI with the original attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String term_uri) {
    String externalID = term_uri;
    if (term_uri.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("term query " + externalID);
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("pesi");
    PESIRecord rec = this.pesidao.getPESIRecordbyGUID(externalID);
    PESIRecord[] synonyms = this.pesidao.getPESISynonyms(externalID);
    Distribution[] dist = this.pesidao.getPESIDistributionsByGUID(externalID);
    Vernacular[] vernaculars = this.pesidao.getPESIVernacularByGUID(externalID);
    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
    if (rec != null) {
      String combinedName = combineName(rec.getScientificname(), rec.getAuthority());
      String acceptedName = combineName(rec.getValid_name(), rec.getValid_authority());
      e.setOriginalTermInfo("combinedName", combinedName.trim());
      e.setOriginalTermInfo("kingdom", rec.getKingdom());
      e.setOriginalTermInfo("guid", rec.getGUID());
      e.setOriginalTermInfo("status", rec.getStatus());
      e.setOriginalTermInfo("rank", rec.getRank());
      if (synonyms != null) {
        ArrayList<String> synList = new ArrayList<String>();
        for (PESIRecord synonym : synonyms) {
          String synonymName = combineName(synonym.getScientificname(), synonym.getAuthority());
          synList.add(synonymName);
        }
        e.setOriginalTermInfo("SynonymisedName", synList);
      }
      if (vernaculars != null) {
        ArrayList<String> vernList = new ArrayList<String>();
        for (Vernacular vernacular : vernaculars) {
          if (vernacular.getVernacular() != null) {
            vernList.add(vernacular.getVernacular());
          }
        }
        e.setOriginalTermInfo("Vernacular", vernList);
      }
      if (!acceptedName.equals(combinedName)) {
        e.setOriginalTermInfo("AcceptedName", acceptedName);
      }
      if (!rec.getValid_guid().equals(rec.getGUID())) {
        e.setOriginalTermInfo("AcceptedGUID", rec.getValid_guid());
      }
      if (dist != null) {
        ArrayList<String> distList = new ArrayList<String>();
        for (Distribution distribution : dist) {
          if (distribution.getLocality() != null) {
            distList.add(distribution.getLocality());
          }
        }
        e.setOriginalTermInfo("Distribution", distList);
      }
      e.setOriginalTermInfo("Citation", rec.getCitation());
    }
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getTermInfosProcessed method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String term_uri) {
    String externalID = term_uri;
    if (term_uri.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("term query " + externalID);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("pesi");
    PESIRecord rec = this.pesidao.getPESIRecordbyGUID(externalID);
    Vernacular[] vernaculars = this.pesidao.getPESIVernacularByGUID(externalID);
    SearchResultSetEntry e = new SearchResultSetEntry();
    if (rec != null) {
      String combinedName = combineName(rec.getScientificname(), rec.getAuthority());
      e.setLabel(combinedName.trim());
      e.setExternalID(rec.getGUID());
      e.setKingdom(rec.getKingdom());
      e.setStatus(rec.getStatus());
      e.setUri(rec.getUrl());
      e.setRank(rec.getRank());
      e.setSourceTerminology(getAcronym());
      if (vernaculars != null) {
        for (Vernacular vernacular : vernaculars) {
          if (vernacular.getVernacular() != null) {
            e.setCommonName(vernacular.getVernacular());
          }
        }
      }
      if (rec.getGUID() != null) {
        if (rec.getGUID().startsWith("urn")) {
          if (rec.getGUID().toLowerCase().contains("faunaeur")) {
            e.setEmbeddedDatabase("Fauna Europaea");
          } else if (rec.getGUID().toLowerCase().contains("marinespecies")) {
            e.setEmbeddedDatabase("European Register of Marine Species");
          } else if (rec.getGUID().toLowerCase().contains("indexfungorum")) {
            e.setEmbeddedDatabase("Index Fungorum");
          }
        } else if (rec.getGUID().toLowerCase().split("-").length == 5) {
          e.setEmbeddedDatabase("Euro+Med PlantBase");
        }
      }
    }
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getTermInfosCombined method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes when mapped and original attributes when not
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("term query " + externalID);
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("pesi");
    PESIRecord rec = this.pesidao.getPESIRecordbyGUID(externalID);
    PESIRecord[] synonyms = this.pesidao.getPESISynonyms(externalID);
    Distribution[] dist = this.pesidao.getPESIDistributionsByGUID(externalID);
    Vernacular[] vernaculars = this.pesidao.getPESIVernacularByGUID(externalID);
    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
    if (rec != null) {
      String combinedName = combineName(rec.getScientificname(), rec.getAuthority());
      String acceptedName = combineName(rec.getValid_name(), rec.getValid_authority());
      e.setLabel(combinedName.trim());
      e.setKingdom(rec.getKingdom());
      e.setExternalID(rec.getGUID());
      e.setStatus(rec.getStatus());
      e.setUri(rec.getUrl());
      e.setRank(rec.getRank());
      if (synonyms != null) {
        ArrayList<String> synList = new ArrayList<String>();
        for (PESIRecord synonym : synonyms) {
          String synonymName = combineName(synonym.getScientificname(), synonym.getAuthority());
          synList.add(synonymName);
        }
        e.setSynonyms(synList);
      }
      if (vernaculars != null) {
        ArrayList<String> vernList = new ArrayList<String>();
        for (Vernacular vernacular : vernaculars) {
          if (vernacular.getVernacular() != null) {
            vernList.add(vernacular.getVernacular());
          }
        }
        e.setCommonNames(vernList);
      }
      if (rec.getGUID() != null) {
        if (rec.getGUID().startsWith("urn")) {
          if (rec.getGUID().toLowerCase().contains("faunaeur")) {
            e.setEmbeddedDatabase("Fauna Europaea");
          } else if (rec.getGUID().toLowerCase().contains("marinespecies")) {
            e.setEmbeddedDatabase("European Register of Marine Species");
          } else if (rec.getGUID().toLowerCase().contains("indexfungorum")) {
            e.setEmbeddedDatabase("Index Fungorum");
          }
        } else if (rec.getGUID().toLowerCase().split("-").length == 5) {
          e.setEmbeddedDatabase("Euro+Med PlantBase");
        }
      }
      e.setSourceTerminology(getAcronym());
      if (!acceptedName.equals(combinedName)) {
        e.setOriginalTermInfo("AcceptedName", acceptedName);
      }
      if (!rec.getValid_guid().equals(rec.getGUID())) {
        e.setOriginalTermInfo("AcceptedGUID", rec.getValid_guid());
      }
      if (dist != null) {
        ArrayList<String> distList = new ArrayList<String>();
        for (Distribution distribution : dist) {
          if (distribution.getLocality() != null) {
            distList.add(distribution.getLocality());
          }
        }
        e.setOriginalTermInfo("Distribution", distList);
      }
      e.setOriginalTermInfo("Citation", rec.getCitation());
    }
    rs.addEntry(e);
    return rs;
  }

  @Override
  public boolean isResponding() {
    PESINameServiceLocator locator = new PESINameServiceLocator();
    PESINameServicePortType port;
    try {
      port = locator.getPESINameServicePort();
      port.getPESIRecordByGUID("12345");
    } catch (RemoteException e) {
      e.printStackTrace();
      return false;
    } catch (ServiceException e1) {
      e1.printStackTrace();
      return false;
    }
    return true;
  }

  /**
   * Implements the search method of the WSInterface Searches for names and common names in the
   * database based on the match type "exact" or "included"
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> search(final String query, final String match_type) {
    LOGGER.info("search query " + query + " matchtype " + match_type);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("pesi");
    PESIRecord[] records;
    if (match_type.equals(SearchTypes.exact.name())) {
      records = this.pesidao.searchByNameExact(query);
    } else {
      records = this.pesidao.searchByNameIncluded(query);
    }
    if (records != null && records[0] != null) {
      for (PESIRecord pr : records) {
        setEntry(rs, pr);
      }
    }
    records = this.pesidao.getPESIVernacular(query);
    if (records != null && records[0] != null) {
      for (PESIRecord pr : records) {
        setEntry(rs, pr);
      }
    }
    LOGGER.info("search query processed " + query + " matchtype " + match_type);
    return rs;
  }

  private void setEntry(GFBioResultSet<SearchResultSetEntry> rs, final PESIRecord sName) {
    String combinedName = combineName(sName.getScientificname(), sName.getAuthority());
    SearchResultSetEntry e = new SearchResultSetEntry();
    e.setLabel(combinedName.toString());
    e.setKingdom(sName.getKingdom());
    e.setRank(sName.getRank());
    e.setStatus(sName.getStatus());
    e.setUri(sName.getUrl());
    e.setExternalID(sName.getGUID());
    e.setSourceTerminology(getAcronym());
    e.setInternal("false");
    if (sName.getGUID() != null) {
      if (sName.getGUID().startsWith("urn")) {
        if (sName.getGUID().toLowerCase().contains("faunaeur")) {
          e.setEmbeddedDatabase("Fauna Europaea");
        } else if (sName.getGUID().toLowerCase().contains("marinespecies")) {
          e.setEmbeddedDatabase("European Register of Marine Species");
        } else if (sName.getGUID().toLowerCase().contains("indexfungorum")) {
          e.setEmbeddedDatabase("Index Fungorum");
        }
      } else if (sName.getGUID().split("-").length == 5) {
        e.setEmbeddedDatabase("Euro+Med PlantBase");
      }
      Vernacular[] vernaculars = this.pesidao.getPESIVernacularByGUID(sName.getGUID());
      if (vernaculars != null) {
        ArrayList<String> vernacularList = new ArrayList<String>();
        for (Vernacular vernacular : vernaculars) {
          String vern = vernacular.getVernacular();
          if (vern != null) {
            vernacularList.add(vernacular.getVernacular());
          }
        }
        if (!vernacularList.isEmpty()) {
          e.setCommonNames(vernacularList);
        }
      }
    }
    rs.addEntry(e);
  }

  @Override
  public boolean supportsMatchType(String matchtype) {
    return Arrays.stream(wsConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(matchtype)::equals);
  }

  private String combineName(String name, String authority) {
    StringBuilder fullname = new StringBuilder();
    if (name != null) {
      fullname.append(name);
    }
    if (!name.equals("") && authority != null) {
      fullname.append(" " + authority);
    }
    return fullname.toString();
  }

  /**
   * Implements the getMetadata method of the WSInterface Gets the metadata information about the
   * webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("pesi");
    MetadataResultSetEntry e = new MetadataResultSetEntry();
    e.setName(getName());
    e.setAcronym(getAcronym());
    e.setVersion(getCurrentVersion());
    e.setDescription(getDescription());
    e.setKeywords(wsConfig.getKeywords());
    e.setUri(getURI());
    e.setContact(wsConfig.getContact());
    e.setContribution(wsConfig.getContribution());
    e.setStorage(wsConfig.getStorage());
    ArrayList<String> taxonLSIDPrefixes = new ArrayList<String>();
    taxonLSIDPrefixes.add("urn:lsid:faunaeur.org:taxname:");
    taxonLSIDPrefixes.add("urn:lsid:marinespecies.org:taxname:");
    e.setNamespaces(taxonLSIDPrefixes);

    ArrayList<String> domainUris = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      domainUris.add(d.getUri());
    }
    e.setDomain(domainUris);
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getCapabilities method of the WSInterface Returns the available service
   * endpoints for the webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("geonames");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : wsConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : wsConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }
}
