package org.gfbio.pesiwebservice;

import java.rmi.RemoteException;
import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;

public class PESIDAO {

  private static final Logger LOGGER = Logger.getLogger(PESIDAO.class);
  private PESINameServiceLocator locator;
  private PESINameServicePortType port;
  public final int waitTime = 1000;

  public PESIDAO() {
    init();
  }

  private void init() {

    locator = new PESINameServiceLocator();
    try {
      port = locator.getPESINameServicePort();

    } catch (ServiceException e) {

      e.printStackTrace();
    }
  }

  public PESIRecord[] searchByNameExact(String scientificname) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESIRecords(scientificname, false, 0);
      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + scientificname, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + scientificname);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + scientificname, e);
        }
      }
    }
  }

  public PESIRecord[] searchByNameIncluded(String scientificname) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESIRecords(scientificname, true, 0);
      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + scientificname, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + scientificname);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + scientificname, e);
        }
      }
    }
  }

  public PESIRecord[] getPESISynonyms(String GUID) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESISynonymsByGUID(GUID, 0);

      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + GUID, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + GUID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + GUID, e);
        }
      }
    }
  }

  public PESIRecord[] getPESIVernacular(String name) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESIRecordsByVernacular(name, 0);

      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + name, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + name);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + name, e);
        }
      }
    }
  }

  public PESIRecord getPESIRecordbyGUID(String GUID) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESIRecordByGUID(GUID);

      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + GUID, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + GUID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + GUID, e);
        }
      }
    }
  }

  public Vernacular[] getPESIVernacularByGUID(String GUID) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESIVernacularsByGUID(GUID, 0);

      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + GUID, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + GUID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + GUID, e);
        }
      }
    }
  }

  public Distribution[] getPESIDistributionsByGUID(String GUID) {

    for (int tries = 0;; tries++) {
      try {
        return port.getPESIDistributionsByGUID(GUID, 0);

      } catch (RemoteException re) {
        if (tries >= 3) {
          LOGGER.warn("match failed, PESI or the connection to lame " + GUID, re);
          return null; // dont return empty because the port returns
                       // also null and
          // the next filter step checks for null
        }

        try {
          LOGGER.warn("a match try failed, wait and next try " + GUID);
          Thread.sleep(waitTime);

        } catch (InterruptedException e) {
          LOGGER.warn(" external interrupted" + GUID, e);
        }
      }
    }
  }
}
